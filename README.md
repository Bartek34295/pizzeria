# README #

### Content of this file ###

* Introduction
* Installation
* How to use?
* Contact

### Introduction ###

Pizzeria project is a program used to help restaurants receive orders and create receipts. It could be utilized on websites and mobile apps to automize ordering process in order to skip queues and allow the restaurant to take more orders.

### Installation ###

Download all the files provided and create a file. It will be named menu.txt, containing products names, prices and type of the product. It has to be in certain folder (D:\\) but you can change the destination in code. 

### How to use the program? ###

Before ordering, client is informed about current promotions that will be added to receipt. Client is capable of choosing from different pizza flavors and drinks. After selecting certain pizza type, the program gives an option to order half-and-half pizza and continues to receive new products. To finish your order, simply type 0 and the receipt will generate into file. By default, menu file is filled with 10 products just to showcase usage of the program, but it can be changed manually although remember to follow structure that is used in the original file, otherwise it won't work

### Contact ###

In case of any problems contact Bartosz Kmiecik or Jakub Miś.