package com.company;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private Double price;
    private List<String> listOfItems = new ArrayList<>();

    /**
     * Klasa <code>Order</code> przechowuje informacje o całym zamówieniu.
     * Zmienna <code>price</code> przechowuje całkowitą cenę zamówienia.
     * Lista <code>listOfItems</code> przechowywuje nazwy zamówionych przedmiotów.
     */

    Order(){
        this.price = 0.0;
    }

    public void addItemToList(String name, double price){
        listOfItems.add(name);
        this.price += price;
    }

    public Double getPrice(){
        return price;
    }

    public List getListOfItems(){
        return listOfItems;
    }
}
