package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Jakub Miś, Bartosz Kmiecik
 */

public class Main {

    public static void main(String[] args) throws IOException {

        List<ItemInMenu> menu = new ArrayList<>();

        /**
         * Główna metoda klasy.
         * Lista <code>menu</code> służy do przechowywania menu. Każdy jej element to inny obiekt klasy <code>MenuItem</code>.
         */

        try (BufferedReader br = new BufferedReader(new FileReader("D:\\menu.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lineSplit = line.split(";");
                ItemInMenu mi = new ItemInMenu(lineSplit[0], lineSplit[1], lineSplit[2], lineSplit[3]);
                menu.add(mi);
            }
        }

        /**
         * Menu zostało zapisane w pliku o nazwie <code>menu.txt</code>, który domyślnie znajduje się na dysku D.
         * Każda linijka tego pliku zawiera inny przedmiot z menu. Poszczególne informacje zostału oddzielone średnikami.
         */

        Order order = new Order();
        Scanner scan = new Scanner(System.in);
        System.out.println("Aby wybrać przedmiot wpisz jego numer. Aby przejść do realizacji zamówienia wpisz 0 (zero)");
        int next; // Zmienna przechowuje numer następnego wybranego produktu.
        int pizzaCounter = 0; // Zmienna licząca pizze do promocji.
        while(true){
            displayMenu(menu);
            System.out.println("Wpisz numer: ");
            next = scan.nextInt();
            if(next != 0){
                if(next < 0 || next > menu.size()){
                    System.out.println("Wprowadzono niepoprawny numer.");
                }else{
                    if (menu.get(next - 1).getType() == 1){
                        System.out.println("Pół na pół (1 - tak, 0 - nie)?: ");
                        int pnp; // Zmienna przechowująca informację o tym, jaką pizzę chce użytkownik.
                        do{
                            pnp = scan.nextInt();
                        }while(pnp != 0 && pnp != 1);
                        switch (pnp){
                            case 0: // Zwykła pizza.
                                order.addItemToList(menu.get(next - 1).getName(), menu.get(next - 1).getPrice());
                                break;
                            case 1: // Pizza pół na pół. Cena to suma połowy cen obu pizz.
                                String pizzaName = menu.get(next - 1).getName();
                                double pizzaPrice = menu.get(next - 1).getPrice() / 2;
                                System.out.println("Podaj numer drugiej pizzy: ");
                                int nr; // Zmienna przechowywująca numer drugiej połówki pizzy.
                                boolean correct = false;
                                do{
                                    nr = scan.nextInt();
                                    if (menu.get(nr - 1).getType() == 1){
                                        correct = true;
                                        pizzaName += "/" + menu.get(nr - 1).getName();
                                        pizzaPrice += menu.get(nr - 1).getPrice() / 2;
                                        order.addItemToList(pizzaName, pizzaPrice);
                                    }else{
                                        System.out.println("Miała być PIZZA!");
                                    }
                                }while (correct == false);
                                break;
                        }
                        pizzaCounter++;
                    }else{
                        order.addItemToList(menu.get(next - 1).getName(), menu.get(next - 1).getPrice()); // Zamawianie przedmiotu nie będącego pizzą.
                    }
                }
                System.out.println("Dodano do zamówienia. Wybierz kolejny przedmiot lub zakończ wpisuhjąc 0 (zero).");
            }else{
                break;
            }
        }

        if(pizzaCounter >= 2){
            System.out.println("Przy zakupie minimum 2 pizz otrzymujesz napój gratis. Wybierz nr napoju z menu:");
            displayMenu(menu);
            int drink;
            while(true){
                drink = scan.nextInt();
                if (drink < 1 || drink > menu.size()){
                    System.out.println("Podano nieprawidłowy numer");
                }else {
                    if (menu.get(drink - 1).getType() == 1){
                        System.out.println("Podano nieprawidłowy numer");
                    }else{
                        order.addItemToList(menu.get(drink - 1).getName(), 0.0);
                        break;
                    }
                }
            }
        }

        Receipt r = new Receipt(order);
        System.out.println("Dziękujemy za złożenie zamówienia.");

    }

    static void displayMenu(List m){
        System.out.println("Lp.\t\t\t\t\tNazwa\t\t\t\t\tCena");
        for(int i = 0; i < m.size(); i++){
            ItemInMenu iim = (ItemInMenu) m.get(i);
            System.out.println(iim.getIndex() + "\t\t\t\t\t" + iim.getName() + "\t\t\t\t\t" + iim.getPrice() + "zł");
        }
    }

    /**
     * Funkcja odpowiada za wyświetlanie menu w konsoli.
     */
}
