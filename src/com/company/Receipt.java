package com.company;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Receipt {

    private Order order;
    private String fileName = "D:\\paragon.txt";
    private Double finalPrice;

    /**
     * Klasa odpowiada za finalizację zamówienia i wygenerowanie paragonu.
     * @param order Zamówienie do finalizacji.
     * Zmienna <code>fileName</code> przechowuje nazwę pliku, do którego ma zostać zapisany paragon.
     * Zmienna <code>finalPrice</code> przechowije wartość całego zamówienia uqwzględniając promocje.
     */

    Receipt(Order order){
        this.order = order;
        if(this.order.getPrice() > 100.0){
            finalPrice = this.order.getPrice() - this.order.getPrice() * 20 / 100;
        }else{
            finalPrice = this.order.getPrice();
        }
        createFile();
        writeToFile();
    }

    private void createFile(){
        try {
            File myObj = new File(fileName);
            if (myObj.createNewFile()) {
                System.out.println("Wygenerowano paragon");
            } else {
                System.out.println("Plik już istnieje");
            }
        } catch (IOException e) {
            System.out.println("Nie udało się utworzyć pliku.");
            e.printStackTrace();
        }
    }

    /**
     * Funkcja odpowiada za tworzenie pliku, w którym zapisany zostanie paragon.
     */

    private void writeToFile(){
        try {
            FileWriter myWriter = new FileWriter(fileName);
            for (int i = 0; i < order.getListOfItems().size(); i ++){
                myWriter.write(order.getListOfItems().get(i) + "\n");
            }
            myWriter.write("Do zapłaty: " + finalPrice + "zł");
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * Funkcja odpowiada za wygenerowanie paragonu, zapisanie go do pliku i naliczenie zniżek.
     */

}
