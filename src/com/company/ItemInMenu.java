package com.company;

public class ItemInMenu {
    private String index;
    private String name;
    private String price;
    private int type;

    /**
     * Klasa <code>MenuItem</code> odpowiada za przechowywanie informacji o przedmiocie dostępnym w menu.
     * @param index Pozycja w menu.
     * @param name Nazwa przedmiotu.
     * @param price Cena.
     * @param type Typ obiektu (1 - pizza, 2 - napój)
     */

    ItemInMenu(String index, String name, String price, String type){
        this.index = index;
        this.name = name;
        this.price = price;
        this.type = Integer.parseInt(type);
    }

    public Integer getIndex() {
        return Integer.parseInt(index);
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return Double.parseDouble(price);
    }

    public int getType() {
        return type;
    }

    /**
     * Metody <code>getIndex()</code> i <code>getPrice()</code> zmieniają typ danych na taki, który ułatwi ich późniejsze przetwarzanie.
     */
}
